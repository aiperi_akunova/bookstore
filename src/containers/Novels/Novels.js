import React from 'react';
import BookBox from "../../components/BookBox/BookBox";

const Novels = () => {
    return (
        <div className='bestseller'>
            <h3>Best Novels you could ever read.</h3>
            <p>Literary critics, historians, avid readers, and even casual readers will all have different opinions on
                which novel is truly the “greatest book ever written.” Is it a novel with beautiful, captivating
                figurative language? Or one with gritty realism? A novel that has had an immense social impact? Or one
                that has more subtly affected the world?</p>
            <div className='best-books'>
                < BookBox
                    image = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQG8ZKnKNXOQH7PcwP9gp0yL8-YkrYy9bemqw&usqp=CAU'
                    title="To Kill a Mockingbird"
                    year='1960'
                    price='25'
                />
                < BookBox
                    image='https://images-na.ssl-images-amazon.com/images/I/41kAxbOhV8L._SX328_BO1,204,203,200_.jpg'
                    title="Jane Eyre"
                    year='1847'
                    price='55'
                />
                < BookBox
                    image='https://m.media-amazon.com/images/I/51tiK-eB3JL.jpg'
                    title="Pride and Prejudice"
                    year='1813'
                    price='45'
                />
            </div>
        </div>
    );
};

export default Novels;