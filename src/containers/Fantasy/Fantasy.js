import React from 'react';
import BookBox from "../../components/BookBox/BookBox";

const Fantasy = () => {
    return (
        <div className='bestseller'>
            <h3>Fantasy might be the best genre.</h3>
            <p>Imagination is one of the greatest delights of human nature. Imagination is the most important foundation
                to the genre of fantasy. I realize that most of the films and books that I have watched and love are all
                under the category of fantasy.</p>
            <div className='best-books'>
                < BookBox
                    image ='https://anylang.net/sites/default/files/covers/9780007286416.jpg'
                    title="Mary Poppins"
                    year='1934'
                    price='30'
                />
                < BookBox
                    image='https://images-na.ssl-images-amazon.com/images/I/81lQFj4fVRL.jpg'
                    title="City of Glass"
                    year='2009'
                    price='35'
                />
                < BookBox
                    image='https://m.media-amazon.com/images/I/51STZt9kl5L.jpg'
                    title="Ozma of Oz"
                    year='1907'
                    price='60'
                />
            </div>
        </div>
    );
};

export default Fantasy;