import React from 'react';
import './Bestseller.css';
import BookBox from "../../components/BookBox/BookBox";

const Bestseller = () => {

    return (
        <div className='bestseller'>
            <h3>Best-selling books of all time</h3>
            <p>We are pretty sure you have heard about
                these books at least on time. Because these books are
                famous all over the world. Here you are some of them!</p>
            <div className='best-books'>
                < BookBox
                    image = 'https://embed.cdn.pais.scholastic.com/v1/channels/sso/products/identifiers/isbn/9780545583008/primary/renditions/700?useMissingImage=true'                        title = "Harry Potter collection"
                    year = '1997-2007'
                    price = '200'
                />
                < BookBox
                    image = 'https://images-na.ssl-images-amazon.com/images/I/81jtrIKJd2L.jpg'
                    title = "The Da Vinci Code"
                    year = '2003'
                    price = '20'
                />
                < BookBox
                    image = 'https://i.ebayimg.com/images/g/E3MAAOSw4CFYohUE/s-l640.jpg'
                    title  = "Angels and Demons"
                    year = '2000'
                    price = '18'
                />
            </div>
        </div>
    );
};

export default Bestseller;