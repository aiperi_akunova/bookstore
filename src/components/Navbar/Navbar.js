import React from 'react';
import './Navbar.css';
import {NavLink} from "react-router-dom";

const Navbar = () => {
    return (
        <div className='navbar'>
            <NavLink activeClassName='active' exact to='/bestseller'>
                Bestsellers
            </NavLink>

            <NavLink activeClassName='active' exact to='/novels'>
                Novels
            </NavLink>

            <NavLink activeClassName='active' exact to='/fantasy'>
                Fantasy
            </NavLink>
        </div>
    );
};

export default Navbar;