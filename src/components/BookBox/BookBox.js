import React from 'react';
import './BookBox.css';

const BookBox = props => {
    return (
        <div className="book-card">
            <img src={props.image}/>
            <h3 className='title'>{props.title}</h3>
            <h3 className='year'>Year: {props.year}</h3>
            <h3 className='price'>Price: <span>{props.price} $</span></h3>
        </div>
    );
};

export default BookBox;