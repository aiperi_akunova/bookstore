import './App.css';
import Bestseller from "./containers/Bestseller/Bestseller";
import React from "react";
import Navbar from "./components/Navbar/Navbar";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Novels from "./containers/Novels/Novels";
import Fantasy from "./containers/Fantasy/Fantasy";

const App = () => {
  return (
      <div className="App">
          <div className='main-page'>
              <h1>Welcome to our bookstore :)</h1>
              <p>Here you can find everything you want to read about!</p>
          </div>
          <BrowserRouter>
          <Navbar/>
          <hr></hr>
            <Switch>
                <Route path='/bestseller' exact component={Bestseller}/>
                <Route path='/novels' component ={Novels}/>
                <Route path='/fantasy' component ={Fantasy}/>
            </Switch>
        </BrowserRouter>
      </div>
  );
};

export default App;
